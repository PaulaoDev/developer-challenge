const winston = require("winston");

module.exports = (clientElk) => {
  const winstonLogger = winston.createLogger({
    level: "info",
    format: winston.format.json(),
    transports: [],
  });

  winstonLogger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    })
  );

  const sendDataToElastic = (body) =>
    new Promise((resolve, reject) => {
      clientElk
        .index({
          index: "app-logs",
          body,
        })
        .then((data) => resolve(data))
        .catch((error) => reject(error));
    });

  const logger = {
    factory(type = "info", message = "", data = []) {
      if (winstonLogger[type]) {
        const sendData = { type, message, data };

        winstonLogger[type](JSON.stringify({ message, data }));

        sendDataToElastic(sendData).catch((err) => console.error(err));
      }
    },
    info(message, ...args) {
      this.factory("info", message, args);
    },
    debug(message, ...args) {
      this.factory("debug", message, args);
    },
    error(message, ...args) {
      this.factory("error", message, args);
    },
  };

  return logger;
};
