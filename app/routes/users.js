const express = require("express");

const router = express.Router();
const api = require("../utils/axios");

module.exports = (clientElk, logger) => {
  router.get("/users", async (req, res) => {
    logger.info("Iniciando metodo /users");

    const { data: pessoas = [] } = await api
      .get("/users")
      .catch((error) =>
        logger.error("Ocorreu um erro a consulta da api externa: ", { error })
      );

    logger.info("Response da api externa: ", { response: pessoas });

    const ordemAlfabeticaFN = (
      { name: nameUm = "" },
      { name: nameDois = "" }
    ) => {
      if (nameUm < nameDois) {
        return -1;
      }
      return 1;
    };

    const suiteFilter = ({ address: { suite = "" } }) =>
      suite.toLowerCase().includes("suite");

    const dataResponse = pessoas.sort(ordemAlfabeticaFN).filter(suiteFilter);

    logger.info("Response final com todos os filtros: ", {
      response: dataResponse,
    });

    res.header("Content-Type", "application/json");
    res.send(JSON.stringify(dataResponse, null, 4));
  });

  router.get("/logs", async (req, res) => {
    logger.info("Iniciando metodo /logs");

    const {
      body: { hits: logs = [] },
    } = await clientElk
      .search({
        index: "app-logs",
      })
      .catch((error) =>
        logger.error("Ocorreu um erro a consulta do elasticsearch: ", { error })
      );

    logger.info("Response do elasticsearch: ", { logs });

    res.header("Content-Type", "application/json");
    res.send(JSON.stringify(logs, null, 4));
  });

  return router;
};
