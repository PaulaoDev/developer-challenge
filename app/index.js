require("dotenv").config();
const express = require("express");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger');

const app = express();
const bodyParser = require("body-parser");

const clientElk = require("./utils/elk");
const logger = require("./utils/logger")(clientElk);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/", require("./routes/users")(clientElk, logger));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(8080, () => {
  logger.info("Servidor online na porta 8080");
  logger.info("Swagger online em http://localhost:8080/docs");
});

module.exports = app;