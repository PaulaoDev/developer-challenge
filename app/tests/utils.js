const loggerFN = require('../utils/logger');

describe('Logger', function () {
  describe('catch functions', function () {
  
    it('should save without error', function (done) {
      const elkMock = {
        index: () => Promise.reject({})
      };
      
      let logger = loggerFN(elkMock);
      
      logger.debug("Ola mundo");
      
      done();
      
    });
    
    it('should save without error', function (done) {
      const elkMock = {
        index: () => Promise.reject({})
      };
      
      let logger = loggerFN(elkMock);
      
      logger.error("Ola mundo");
      
      done();
      
    });
    
  });
});