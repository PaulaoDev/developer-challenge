const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');
chai.use(chaiHttp);
chai.should();

describe("Chamadas Rest", () => {
    describe("GET /users", () => {
        
        // Teste para retornar todos usuarios
        it("retorna os dados da api externa", (done) => {
             chai.request(app)
                 .get('/users')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('array');
                     done();
                  });
         });
         
        // Teste para retornar logs
        it("retorna todos logs do elastichsearch", (done) => {
             chai.request(app)
                 .get('/logs')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     done();
                  });
         });
         
    });
});