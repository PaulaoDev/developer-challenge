FROM node:current-slim

# Create app directory
WORKDIR /usr/app
# Install app dependencies

# Copy app source code
COPY ./app .

RUN npm install -g nodemon
RUN npm install --no-progress

#Expose port and start application
EXPOSE 8080
EXPOSE 9200

RUN npm test

CMD [ "npm", "run", "start" ]